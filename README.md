# README #

Source files for the various templates used in demonstrating the RPG Group Project

### What is in this repository? ###

* The VB.Net Project that Mr Howse created
* A Python random map generator

### Map Textures ###

A collection of free open source textures can be found [here](http://opengameart.org/textures/all).